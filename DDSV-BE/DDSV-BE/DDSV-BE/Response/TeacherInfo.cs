﻿using DDSV_BE.Controllers;
using DDSV_BE.Model.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Response
{
    public class TeacherInfo
    {
        private UserManager<ApplicationUser> AppUserManager;

        public TeacherInfo(UserManager<ApplicationUser> appUserManager)
        {
            AppUserManager = appUserManager;
        }

        public TeacherInfoReturn Create (ApplicationUser appUser, Teacher teacher)
        {
            return new TeacherInfoReturn
            {
                Id = appUser.Id,
                FullName = teacher.FullName,
                UserName = appUser.UserName,
                DateOfBirth = teacher.DateOfBirth,
                DateOfWork = teacher.DateOfWork,
                Gender = teacher.Gender,
                Address = teacher.Address,
                RelativePhoneNumber = teacher.RelativePhoneNumber,
                Roles = AppUserManager.GetRolesAsync(appUser).Result
            };
        }
    }

    public class TeacherInfoReturn
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfWork { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public int? RelativePhoneNumber { get; set; }
        public IList<string> Roles { get; set; }
    }
}
