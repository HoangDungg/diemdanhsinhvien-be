﻿using DDSV_BE.Model.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Response
{
    public class StudentInfo
    {
        private UserManager<ApplicationUser> AppUserManager;

        public StudentInfo(UserManager<ApplicationUser> appUserManager)
        {
            AppUserManager = appUserManager;
        }

        public StudentInfoReturn Create(ApplicationUser appUser, Student student)
        {
            return new StudentInfoReturn
            {
                Id = student.StudentId,
                FullName = student.FullName,
                UserName = appUser.UserName,
                Address = student.Address,
                Gender = student.Gender,
                DateOfBirth = student.DateOfBirth,
                DateOfIntoSchool = student.DateOfIntoSchool,
                MotherPhone = student.MotherPhone,
                FatherPhone = student.FatherPhone,
                Roles = AppUserManager.GetRolesAsync(appUser).Result
            };
        }
    }

    public class StudentInfoReturn
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfIntoSchool { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public int? MotherPhone { get; set; }
        public int? FatherPhone { get; set; }
        public IList<string> Roles { get; set; }
    }
}
