﻿using DDSV_BE.Model.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Response
{
    public class AdminInfo
    {
        private UserManager<ApplicationUser> AppUserManager;

        public AdminInfo(UserManager<ApplicationUser> appUserManager)
        {
            AppUserManager = appUserManager;
        }

        public AdminInfoReturn Create(ApplicationUser appUser, Admin admin)
        {
            return new AdminInfoReturn
            {
                Id = appUser.Id,
                FullName = admin.FullName,
                UserName = appUser.UserName,
                Email = appUser.Email,
                PhoneNumber = appUser.PhoneNumber,
                DateOfBirth = admin.DateOfBirth,
                Address = admin.Address,
                Roles = AppUserManager.GetRolesAsync(appUser).Result
            };
        }
    }

    public class AdminInfoReturn
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DateOfBirth { get; set; }
        public IList<string> Roles { get; set; }
    }
}
