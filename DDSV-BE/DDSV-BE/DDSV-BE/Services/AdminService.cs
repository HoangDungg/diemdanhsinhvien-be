﻿using DDSV_BE.Helpers;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DDSV_BE.Services
{
    public interface IAdminService
    {
        Task<IdentityResult> Create(ApplicationUser admin, string password);
        Task<(ApplicationUser user,Admin admin, string token, HttpStatusCode sttCode, string msg)> Login(int username, string password);
        Task<ApplicationUser> FindById(string id = null);
        Task<ApplicationUser> GetCurrentAdmin();
        Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update(UpdateProfileBindingModel adminData, string id = null);
        Task<IdentityResult> Delete(string id);
        Task<IdentityResult> ChangePassword(ApplicationUser admin, string oldPass, string newPass);
        Task<(IdentityResult result, string newPassword)> ResetPassword(string id);
        Task<ApplicationUser> FindByUserName(string username);
    }
    public class AdminService : IAdminService
    {
        private UserManager<ApplicationUser> userManager;
        private RoleManager<Roles> roleManager;
        private SignInManager<ApplicationUser> signInManager;
        private readonly AppSettings appSettings;
        private IHttpContextAccessor httpContextAccessor;
        private ddsvContext db;
        public AdminService(ddsvContext db, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
                            IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor, RoleManager<Roles> roleManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.appSettings = appSettings.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.roleManager = roleManager;
        }

        public async Task<IdentityResult> Create(ApplicationUser admin, string password)
        {
            IdentityResult result = await this.userManager.CreateAsync(admin, password);
            if (result.Succeeded)
            {
                bool x = await this.roleManager.RoleExistsAsync("Admin");
                if(!x)
                {
                    var role = new Roles();
                    role.Name = "Admin";
                    await this.roleManager.CreateAsync(role);
                }
                await this.userManager.AddToRoleAsync(admin,"Admin");
                IdentityResult claimResult = await userManager.AddClaimAsync(admin, new Claim(ClaimTypes.Role, Role.admin));
            }
            return result;
        }

        public async Task<(ApplicationUser user,Admin admin, string token, HttpStatusCode sttCode, string msg)> Login(int username, string password)
        {
            ApplicationUser user = await FindByUserName(username.ToString());
            Admin admin = db.Admins.FirstOrDefault(a => a.adminId == username);
            if (admin == null)
            {
                return (user, admin,"", HttpStatusCode.NotFound, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var signInResult = await signInManager.PasswordSignInAsync(username.ToString(), password, false, false);

            if (!signInResult.Succeeded)
            {
                return (null,null, "", HttpStatusCode.BadRequest, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var userRole = userManager.GetClaimsAsync(user).Result.Where(claim => claim.Type == ClaimTypes.Role);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, admin.Id.ToString()),
                    new Claim(ClaimTypes.Role, Role.admin)
                }),
                Expires = DateTime.UtcNow.AddDays(0.5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return (user,admin, tokenString, 0, null);
        }

        public async Task<ApplicationUser> FindById(string id = null)
        {
            return string.IsNullOrEmpty(id) ? await GetCurrentAdmin() : await this.userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> FindByUserName(string username)
        {
            return await this.userManager.FindByNameAsync(username);
        }

        public async Task<ApplicationUser> GetCurrentAdmin()
        {
            return await this.userManager.FindByIdAsync(httpContextAccessor.HttpContext.User.Identity.Name);
        }

        public async Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update (UpdateProfileBindingModel adminData, string id = null)
        {
            ApplicationUser user = string.IsNullOrEmpty(id) ? await GetCurrentAdmin() : await this.userManager.FindByNameAsync(id);
            var admin = db.Admins.FirstOrDefault(a => a.adminId.ToString() == user.UserName);
            
            if(user == null)
            {
                return (null, HttpStatusCode.NotFound, "Không Tìm Thấy Người Dùng");
            }

            if(!string.IsNullOrEmpty(adminData.FullName))
            {
                admin.FullName = adminData.FullName;
            }
            else if(!string.IsNullOrEmpty(adminData.DateOfBirth))
            {
                admin.DateOfBirth = adminData.DateOfBirth;
            }
            else if(!string.IsNullOrEmpty(adminData.Address))
            {
                admin.Address = adminData.Address;
            }
            else if(!string.IsNullOrEmpty(adminData.Gender))
            {
                admin.Gender = adminData.Gender;
            }

            user.PhoneNumber = adminData.PhoneNumber;
            IdentityResult result = await this.userManager.UpdateAsync(user);
            return (result, 0, "");
        }

        public async Task<IdentityResult> Delete (string id)
        {
            var admin = await this.userManager.FindByNameAsync(id);
            if(admin != null)
            {
                var user = db.Admins.FirstOrDefault(a => a.adminId.ToString() == admin.UserName);
                if(user != null)
                {
                    db.Admins.Remove(user);
                    db.SaveChanges();
                }
                return await this.userManager.DeleteAsync(admin);
            }
            return null;
        }

        public async Task<IdentityResult> ChangePassword(ApplicationUser admin, string oldPass, string newPass)
        {
            return await this.userManager.ChangePasswordAsync(admin, oldPass, newPass);
        }

        public async Task<(IdentityResult result, string newPassword)> ResetPassword(string id)
        {
            var admin = await this.userManager.FindByNameAsync(id);
            if(admin == null)
            {
                return (null, string.Empty);
            }

            var passToken = userManager.GeneratePasswordResetTokenAsync(admin);
            string newPass = admin.UserName;
            return (await this.userManager.ResetPasswordAsync(admin, passToken.Result, newPass), newPass);
        }

        #region SUPPORT FUNCTION
        #endregion
    }
}
