﻿using DDSV_BE.Helpers;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DDSV_BE.Services
{
    public interface IStudentService
    {
        Task<IdentityResult> Create(ApplicationUser student, string password);
        Task<(ApplicationUser user,Student student, string token, HttpStatusCode sttCode, string errMsg)> Login(int username, string password);
        Task<IdentityResult> Delete(string id);
        Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update(UpdateProfileBindingModel studentData);
        Task<IdentityResult> ChangePassword(ApplicationUser student, string oldPass, string newPass);
        Task<(IdentityResult result, string newPassword)> ResetPassword(string id);
        Task<ApplicationUser> FindByUserName(string username);
        Task<ApplicationUser> FindById(string id = null);
        Task<ApplicationUser> GetCurrentStudent();
    }

    public class StudentService : IStudentService
    {
        private UserManager<ApplicationUser> userManager;
        private RoleManager<Roles> roleManager;
        private SignInManager<ApplicationUser> signInManager;
        private readonly AppSettings appSettings;
        private IHttpContextAccessor httpContextAccessor;
        private ddsvContext db;
        public StudentService(ddsvContext db, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
                            IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor, RoleManager<Roles> roleManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.appSettings = appSettings.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.roleManager = roleManager;
        }

        public async Task<IdentityResult> Create(ApplicationUser student, string password)
        {
            IdentityResult result = await this.userManager.CreateAsync(student, password);
            if (result.Succeeded)
            {
                bool x = await this.roleManager.RoleExistsAsync("Student");
                if (!x)
                {
                    var role = new Roles();
                    role.Name = "Student";
                    await this.roleManager.CreateAsync(role);
                }
                await this.userManager.AddToRoleAsync(student, "Student");
                IdentityResult claimResult = await userManager.AddClaimAsync(student, new Claim(ClaimTypes.Role, Role.student));
            }
            return result;
        }

        public async Task<(ApplicationUser user, Student student, string token, HttpStatusCode sttCode, string errMsg)> Login(int username, string password)
        {
            ApplicationUser user = await FindByUserName(username.ToString());
            Student student = db.Students.FirstOrDefault(s => s.StudentId == username);

            if (student == null)
            {
                return (user,student, "", HttpStatusCode.NotFound, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var signInResult = await signInManager.PasswordSignInAsync(username.ToString(), password, false, false);

            if (!signInResult.Succeeded)
            {
                return (null,null, "", HttpStatusCode.BadRequest, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, student.Id.ToString()),
                    new Claim(ClaimTypes.Role, Role.student)
                }),
                Expires = DateTime.UtcNow.AddDays(0.5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return (user,student, tokenString, 0, null);
        }
        public async Task<ApplicationUser> FindById(string id = null)
        {
            return string.IsNullOrEmpty(id) ? await GetCurrentStudent() : await this.userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> FindByUserName(string username)
        {
            return await this.userManager.FindByNameAsync(username);
        }

        public async Task<ApplicationUser> GetCurrentStudent()
        {
            return await this.userManager.FindByIdAsync(httpContextAccessor.HttpContext.User.Identity.Name);
        }

        public async Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update(UpdateProfileBindingModel studentData)
        {
            ApplicationUser user = string.IsNullOrEmpty(studentData.Id) ? await GetCurrentStudent() : await this.userManager.FindByNameAsync(studentData.Id);
            var student = db.Students.FirstOrDefault(a => a.StudentId.ToString() == user.UserName);

            if (user == null)
            {
                return (null, HttpStatusCode.NotFound, "Không Tìm Thấy Người Dùng");
            }

            if (!string.IsNullOrEmpty(studentData.FullName))
            {
                student.FullName = studentData.FullName;
            }
            else if (!string.IsNullOrEmpty(studentData.DateOfBirth))
            {
                student.DateOfBirth = studentData.DateOfBirth;
            }
            else if (!string.IsNullOrEmpty(studentData.Address))
            {
                student.Address = studentData.Address;
            }
            else if (!string.IsNullOrEmpty(studentData.Gender))
            {
                student.Gender = studentData.Gender;
            }

            user.PhoneNumber = studentData.PhoneNumber;
            IdentityResult result = await this.userManager.UpdateAsync(user);
            return (result, 0, "");
        }

        public async Task<IdentityResult> Delete(string id)
        {
            var student = await this.userManager.FindByNameAsync(id);
            if (student != null)
            {
                var user = db.Students.FirstOrDefault(a => a.StudentId.ToString() == student.UserName);
                if (user != null)
                {
                    db.Students.Remove(user);
                    db.SaveChanges();
                }
                return await this.userManager.DeleteAsync(student);
            }
            return null;
        }

        public async Task<IdentityResult> ChangePassword(ApplicationUser teacher, string oldPass, string newPass)
        {
            return await this.userManager.ChangePasswordAsync(teacher, oldPass, newPass);
        }

        public async Task<(IdentityResult result, string newPassword)> ResetPassword(string id)
        {
            var student = await this.userManager.FindByNameAsync(id);
            if (student == null)
            {
                return (null, string.Empty);
            }

            var passToken = userManager.GeneratePasswordResetTokenAsync(student);
            string newPass = student.UserName;
            return (await this.userManager.ResetPasswordAsync(student, passToken.Result, newPass), newPass);
        }
    }
}
