﻿using DDSV_BE.Helpers;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DDSV_BE.Services
{
    public interface ITeacherService
    {
        Task<IdentityResult> Create(ApplicationUser teacher, string password);
        Task<(ApplicationUser user,Teacher teacher, string token, HttpStatusCode sttCode, string errMsg)> Login(int username, string password);
        Task<IdentityResult> Delete(int id);
        Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update(UpdateProfileBindingModel teacherData);
        Task<IdentityResult> ChangePassword(ApplicationUser admin, string oldPass, string newPass);
        Task<(IdentityResult result, string newPassword)> ResetPassword(string id);
        Task<ApplicationUser> FindByUserName(string username);
        Task<ApplicationUser> FindById(string id = null);
        Task<ApplicationUser> GetCurrentTeacher();
    }

    public class TeacherService : ITeacherService
    {
        private UserManager<ApplicationUser> userManager;
        private RoleManager<Roles> roleManager;
        private SignInManager<ApplicationUser> signInManager;
        private readonly AppSettings appSettings;
        private IHttpContextAccessor httpContextAccessor;
        private ddsvContext db;
        public TeacherService(ddsvContext db, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
                            IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor, RoleManager<Roles> roleManager)
        {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.appSettings = appSettings.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.roleManager = roleManager;
        }

        public async Task<IdentityResult> Create(ApplicationUser teacher, string password)
        {
            IdentityResult result = await this.userManager.CreateAsync(teacher, password);
            if (result.Succeeded)
            {
                bool x = await this.roleManager.RoleExistsAsync("Teacher");
                if (!x)
                {
                    var role = new Roles();
                    role.Name = "Teacher";
                    await this.roleManager.CreateAsync(role);
                }
                await this.userManager.AddToRoleAsync(teacher, "Teacher");
                IdentityResult claimResult = await userManager.AddClaimAsync(teacher, new Claim(ClaimTypes.Role, Role.teacher));
            }
            return result;
        }

        public async Task<(ApplicationUser user,Teacher teacher, string token, HttpStatusCode sttCode, string errMsg)> Login (int username, string password)
        {
            ApplicationUser user = await FindByUserName(username.ToString());
            Teacher teacher = db.Teachers.FirstOrDefault(t => t.TeacherId == username);

            if (teacher == null )
            {
                return (user,teacher, "", HttpStatusCode.NotFound, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var signInResult = await signInManager.PasswordSignInAsync(username.ToString(), password, false, false);

            if (!signInResult.Succeeded)
            {
                return (null, null, "", HttpStatusCode.BadRequest, "Tài khoản hoặc mật khẩu không đúng!");
            }

            var userRole = userManager.GetClaimsAsync(user).Result.Where(claim => claim.Type == ClaimTypes.Role);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, teacher.Id.ToString()),
                    new Claim(ClaimTypes.Role, Role.teacher)
                }),
                Expires = DateTime.UtcNow.AddDays(0.5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return (user,teacher, tokenString, 0, null);
        }
        public async Task<ApplicationUser> FindById(string id = null)
        {
            return string.IsNullOrEmpty(id) ? await GetCurrentTeacher() : await this.userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> FindByUserName(string username)
        {
            return await this.userManager.FindByNameAsync(username);
        }

        public async Task<ApplicationUser> GetCurrentTeacher()
        {
            return await this.userManager.FindByIdAsync(httpContextAccessor.HttpContext.User.Identity.Name);
        }

        public async Task<(IdentityResult result, HttpStatusCode sttCode, string errMsg)> Update(UpdateProfileBindingModel teacherData)
        {
            ApplicationUser user = string.IsNullOrEmpty(teacherData.Id) ? await GetCurrentTeacher() : await this.userManager.FindByNameAsync(teacherData.Id);
            var teacher = db.Teachers.FirstOrDefault(a => a.TeacherId.ToString() == user.UserName);

            if (user == null)
            {
                return (null, HttpStatusCode.NotFound, "Không Tìm Thấy Người Dùng");
            }

            if (!string.IsNullOrEmpty(teacherData.FullName))
            {
                teacher.FullName = teacherData.FullName;
            }
            else if (!string.IsNullOrEmpty(teacherData.DateOfBirth))
            {
                teacher.DateOfBirth = teacherData.DateOfBirth;
            }
            else if (!string.IsNullOrEmpty(teacherData.Address))
            {
                teacher.Address = teacherData.Address;
            }
            else if (!string.IsNullOrEmpty(teacherData.Gender))
            {
                teacher.Gender = teacherData.Gender;
            }

            user.PhoneNumber = teacherData.PhoneNumber;
            IdentityResult result = await this.userManager.UpdateAsync(user);
            return (result, 0, "");
        }

        public async Task<IdentityResult> Delete(int id)
        {
            var user = await this.userManager.FindByNameAsync(id.ToString());
           if(user != null)
            {
                var teacher = db.Teachers.FirstOrDefault(a => a.TeacherId.ToString() == user.UserName);
                if (teacher != null)
                {
                    db.Teachers.Remove(teacher);
                    db.SaveChanges();
                }
                return await this.userManager.DeleteAsync(user);
            }
            return null;
        }

        public async Task<IdentityResult> ChangePassword(ApplicationUser teacher, string oldPass, string newPass)
        {
            return await this.userManager.ChangePasswordAsync(teacher, oldPass, newPass);
        }

        public async Task<(IdentityResult result, string newPassword)> ResetPassword(string id)
        {
            var teacher = await this.userManager.FindByNameAsync(id);
            if (teacher == null)
            {
                return (null, string.Empty);
            }

            var passToken = userManager.GeneratePasswordResetTokenAsync(teacher);
            string newPass = teacher.UserName;
            return (await this.userManager.ResetPasswordAsync(teacher, passToken.Result, newPass), newPass);
        }
    }
}
