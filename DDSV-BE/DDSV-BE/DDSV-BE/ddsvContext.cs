﻿using DDSV_BE.Model;
using DDSV_BE.Model.Attendance;
using DDSV_BE.Model.User;
using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE
{
    public class ddsvContext : IdentityDbContext<ApplicationUser, Roles, int>
    {
        public ddsvContext(DbContextOptions<ddsvContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<RolesDetail>().HasKey(rd => new { rd.roleId, rd.roleFunctionId });
            builder.Entity<ClassListStudents>().HasKey(c => new {c.studentId, c.classListId });
            builder.Entity<SubjectClass>()
                .HasOne<ClassList>(c => c.classList)
                .WithOne(s => s.subjectClass)
                .HasForeignKey<ClassList>(s => s.subjectClassId);
            builder.Entity<SubjectClassStudent>().HasKey(s => new { s.studentId, s.subjectClassId});

            seedData(builder);
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<RoleFunction> RoleFunctions { get; set; }
        public DbSet<RolesDetail> RolesDetails { get; set; }
        public DbSet<ClassList> ClassLists { get; set; }
        public DbSet<ClassListStudents> ClassListStudents { get; set; }
        public DbSet<ClassOfStudent> ClassOfStudents { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectClass> SubjectClasses { get; set; }
        public DbSet<SubjectClassStudent> SubjectClassStudents { get; set; }
        public DbSet<Attendances> Attendances { get; set; }
        //public DbSet<ApplicationUser> User { get; set; }

        private void seedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Faculty>().HasData(
                new Faculty()
                {
                    Id = 1,
                    Name = "Công Nghệ Thông Tin"
                }
            );

            modelBuilder.Entity<Subject>().HasData(
                new Subject() 
                { 
                    Id = 1,
                    subjectId = "CMP223",
                    subjectName = "Lập Trình Web",
                    facultyId = 1,
                }
                );

            //modelBuilder.Entity<SubjectClass>().HasData(
            //    new SubjectClass()
            //    {
            //        Id = 1,
            //        scId = "CNP223 nhóm 2",
            //        scName = "Lập Trình Web",
            //        roomNumber = "E1.02.10",
            //        timeStart = "12:30",
            //        numOfPerios = 7,
            //        belongGroup = "Lý Thuyết",
            //        subjectId = 1,
            //        teacherId = 1
            //    }
            //    );

            //modelBuilder.Entity<ClassList>().HasData(
            //    new ClassList()
            //    {
            //        Id = 1,
            //        subjectClassId = 1
            //    });

            
        }
    }
}
