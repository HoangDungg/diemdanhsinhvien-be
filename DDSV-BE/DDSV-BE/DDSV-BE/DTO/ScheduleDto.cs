﻿using DDSV_BE.Model;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.DTO
{
    public class ScheduleDto
    {
        public int Id { get; set; }
        public string scId { get; set; }
        public string scName { get; set; }
        public string roomNumber { get; set; }
        public string timeStart { get; set; }
        public int numOfPerios { get; set; }
        public int numOfSession { get; set; }
        public string belongGroup { get; set; }
        public DateTime? date { get; set; }

        public int subjectId { get; set; }
        public string subjectName { get; set; }

        public int teacherId { get; set; }
        public string teacherName { get; set; }
        public int classList { get; set; }
        public virtual IEnumerable<int> subjectClassStudents { get; set; }

        public ScheduleDto() { }
        public ScheduleDto(int Id, string scId, string scName, string roomNumber, string timeStart, int numOfPerios, int numOfSession, string belongGroup, int subjectId, string subjectName, int teacherId, string teacherName, int classList, IEnumerable<int> subjectClassStudents, DateTime? date)
        {
            this.Id = Id;
            this.scId = scId;
            this.scName = scName;
            this.roomNumber = roomNumber;
            this.timeStart = timeStart;
            this.numOfPerios = numOfPerios;
            this.numOfSession = numOfSession;
            this.belongGroup = belongGroup;
            this.subjectId = subjectId;
            this.subjectName = subjectName;
            this.teacherId = teacherId;
            this.teacherName = teacherName;
            this.classList = classList;
            this.subjectClassStudents = subjectClassStudents;
            this.date = date;
        }
    }
}
