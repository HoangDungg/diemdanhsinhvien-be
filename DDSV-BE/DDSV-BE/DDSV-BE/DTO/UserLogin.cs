﻿using DDSV_BE.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.DTO.Admin
{
    public class UserLogin 
    {
        public int username { get; set; }
        public string password { get; set; }
    }
}
