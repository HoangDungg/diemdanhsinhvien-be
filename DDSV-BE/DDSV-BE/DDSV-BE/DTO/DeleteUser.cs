﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.DTO
{
    public class DeleteUser
    {
        public int Id { get; set; }
    }
}
