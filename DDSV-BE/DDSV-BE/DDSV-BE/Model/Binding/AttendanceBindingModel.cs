﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding
{
    public class AttendanceBindingModel
    {
        public int classListId { get; set; }
        public int studentId { get; set; }
        public int subjectClassId { get; set; }
        public bool isCheck { get; set; }
    }
}
