﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding.Admin
{
    public class UpdateProfileBindingModel
    {
        public string Id { get; set; }

        [Display(Name = "Họ Tên")]
        public string FullName { get; set; }

        [Display(Name = "Địa Chỉ")]
        public string Address { get; set; }

        [Display(Name = "Ngày Sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Số Điện Thoại")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Display(Name = "Giới Tính")]
        public string Gender { get; set; }
    }
}
