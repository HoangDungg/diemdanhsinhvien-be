﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding.Teacher
{
    public class CreateTeacherBindingModel
    {
        [Display(Name = "Họ Tên")]
        public string FullName { get; set; }

        [Display(Name = "Địa Chỉ")]
        public string Address { get; set; }

        [Display(Name = "Giới Tính")]
        public string Gender { get; set; }

        [Display(Name = "Số Điện Thoại")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật Khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ngày Sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Ngày Vào Làm")]
        public string DateOfWork { get; set; }

        [Display(Name = "Số điện thoại người thân")]
        public int? RelativePhone { get; set; }
    }
}
