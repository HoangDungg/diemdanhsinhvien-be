﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding.Student
{
    public class CreateStudentBindingModel
    {
        [Display(Name = "Họ Tên")]
        public string FullName { get; set; }

        [Display(Name = "Địa Chỉ")]
        public string Address { get; set; }

        [Display(Name = "Giới Tính")]
        public string Gender { get; set; }

        [Display(Name = "Số Điện Thoại")]
        [Phone]
        public string PhoneNumber { get; set; }

        [Display(Name = "Số Điện Thoại Cha")]
        [Phone]
        public string FatherNumber { get; set; }
        
        [Display(Name = "Số Điện Thoại Mẹ")]
        [Phone]
        public string MotherNumber { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật Khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ngày Sinh")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Ngày Vào Học")]
        public string DateOfIntoSchool { get; set; }
    }
}
