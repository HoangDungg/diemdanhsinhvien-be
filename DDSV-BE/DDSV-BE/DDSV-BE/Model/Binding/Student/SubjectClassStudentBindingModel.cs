﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding.Student
{
    public class SubjectClassStudentBindingModel
    {
        public int subjectClassId { get; set; }
        public int studentId { get; set; }
    }
}
