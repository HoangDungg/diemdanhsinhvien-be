﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Binding.Student
{
    public class ClassListStudentBindingModel
    {
        public int classListId { get; set; }
        public int studentId { get; set; }
    }
}
