﻿using DDSV_BE.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class ClassOfStudent
    {
        public ClassOfStudent()
        {
            students = new HashSet<Student>();
        }
        public int Id { get; set; }
        public string classId { get; set; }
        public string className { get; set; }

        public virtual ICollection<Student> students { get; set; }
    }
}
