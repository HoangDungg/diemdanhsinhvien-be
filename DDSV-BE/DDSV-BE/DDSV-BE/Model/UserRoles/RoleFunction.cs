﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.UserRoles
{
    public class RoleFunction
    {
        public RoleFunction()
        {
            roledetail = new HashSet<RolesDetail>();
        }
        [Key]
        public int roleFunctionId { get; set; }
        public string roleFunctionDetail { get; set; }
        public virtual ICollection<RolesDetail> roledetail { get; set; }
    }
}
