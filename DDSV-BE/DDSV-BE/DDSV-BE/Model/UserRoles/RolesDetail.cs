﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.UserRoles
{
    public class RolesDetail
    {
        public int roleId { get; set; }
        public int roleFunctionId { get; set; }
        public bool? Status { get; set; }
        public Roles userRoles { get; set; }
        public RoleFunction roleFunction { get; set; }
    }
}
