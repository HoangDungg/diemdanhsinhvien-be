﻿using DDSV_BE.Model.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.UserRoles
{
    public class Roles : IdentityRole<int>
    {
        public Roles()
        {
            admin = new HashSet<Admin>();
            teacher = new HashSet<Teacher>();
            student = new HashSet<Student>();
            roledetail = new HashSet<RolesDetail>();
        }

        public virtual ICollection<RolesDetail> roledetail { get; set; }
        public virtual ICollection<Admin> admin { get; set; }
        public virtual ICollection<Teacher> teacher { get; set; }
        public virtual ICollection<Student> student { get; set; }
    }
}
