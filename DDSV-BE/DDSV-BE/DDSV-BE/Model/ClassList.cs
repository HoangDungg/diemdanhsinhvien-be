﻿using DDSV_BE.Model.Attendance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class ClassList
    {
        public ClassList()
        {
            classListStudent = new HashSet<ClassListStudents>();
        }
        public int Id { get; set; }
        public int subjectClassId { get; set; }
        public SubjectClass subjectClass { get; set; }
        public virtual ICollection<ClassListStudents> classListStudent { get; set; }
    }
}
