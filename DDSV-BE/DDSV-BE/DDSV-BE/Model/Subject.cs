﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class Subject
    {
        public Subject()
        {
            subjectClass = new HashSet<SubjectClass>();
        }
        public int Id { get; set; }
        public string subjectId { get; set; }
        public string subjectName { get; set; }
        public int facultyId { get; set; }
        public Faculty faculty { get; set; }
        public virtual ICollection<SubjectClass> subjectClass { get; set; }
    }
}
