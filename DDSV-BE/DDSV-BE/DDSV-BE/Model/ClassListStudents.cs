﻿using DDSV_BE.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class ClassListStudents
    {
        public int studentId { get; set; }
        public Student students { get; set; }
        public int classListId { get; set; }
        public  ClassList classList { get; set; }
    }
}
