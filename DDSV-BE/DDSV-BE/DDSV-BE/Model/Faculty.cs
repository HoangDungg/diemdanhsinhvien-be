﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class Faculty
    {
        public Faculty()
        {
            subjects = new HashSet<Subject>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Subject> subjects { get; set; }
    }
}
