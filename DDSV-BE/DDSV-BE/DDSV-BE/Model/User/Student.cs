﻿using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.User
{
    public class Student
    {
        public Student()
        {
            classListStudent = new HashSet<ClassListStudents>();
            subjectClassStudents = new HashSet<SubjectClassStudent>();
        }
        [Key]
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public int? FatherPhone { get; set; }
        public int? MotherPhone { get; set; }
        public string DateOfIntoSchool { get; set; }
        //public int RoleId { get; set; }
        //public Roles userRoles { get; set; }
        public string ClassId { get; set; }
        public ClassOfStudent classOfStudents { get; set; }
        public virtual ICollection<ClassListStudents> classListStudent { get; set; }
        public virtual ICollection<SubjectClassStudent> subjectClassStudents { get; set; }
    }
}
