﻿using DDSV_BE.Model.UserRoles;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.User
{
    public class Teacher 
    {
        public Teacher()
        {
            subjectClasses = new HashSet<SubjectClass>();
        }
        [Key]
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfWork { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public int? RelativePhoneNumber { get; set; }
        public virtual ICollection<SubjectClass> subjectClasses { get; set; }
        //public int RoleId { get; set; }
        //public virtual Roles userRoles { get; set; }
    }
}
