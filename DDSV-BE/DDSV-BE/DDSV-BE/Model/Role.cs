﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public static class Role
    {
        public const string admin = "Admin";
        public const string teacher = "Teacher";
        public const string student = "Student";
    }
}
