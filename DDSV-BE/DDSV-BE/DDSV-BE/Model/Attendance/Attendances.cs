﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model.Attendance
{
    public class Attendances
    {
        public int Id { get; set; }
        public string timeSave { get; set; }
        public int StudentId { get; set; }
        public int classListId { get; set; }
        public ClassListStudents classList { get; set; }
        public bool isCheck { get; set; }
    }
}
