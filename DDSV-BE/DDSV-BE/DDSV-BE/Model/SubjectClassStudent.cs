﻿using DDSV_BE.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class SubjectClassStudent
    {
        public int studentId { get; set; }
        public Student student { get; set; }
        public int subjectClassId { get; set; }
        public SubjectClass subjectClass { get; set; }
    }
}
