﻿using DDSV_BE.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDSV_BE.Model
{
    public class SubjectClass
    {
        public SubjectClass()
        {
            subjectClassStudents = new HashSet<SubjectClassStudent>();
        }
        public int Id { get; set; }
        public string scId { get; set; }
        public string scName { get; set; }
        public string roomNumber { get; set; }
        public string timeStart { get; set; }
        public int numOfPerios { get; set; }
        public int numOfSession { get; set; }
        public string belongGroup { get; set; }
        public DateTime? Date { get; set; }
        public bool isOpen { get; set; }

        public int subjectId { get; set; }
        public Subject subject { get; set; }

        public int teacherId { get; set; }
        public Teacher teacher { get; set; }
        public ClassList classList { get; set; }
        public virtual ICollection<SubjectClassStudent> subjectClassStudents { get; set; }
    }
}
