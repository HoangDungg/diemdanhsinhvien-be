﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDSV_BE.Migrations
{
    public partial class UpdateTableSubjectClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttendanceDetail_ClassList_classListId",
                table: "AttendanceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassList_SubjectClass_scId",
                table: "ClassList");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassListStudents_ClassList_classListId",
                table: "ClassListStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_RolesDetail_RoleFunction_roleFunctionId",
                table: "RolesDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_RolesDetail_AspNetRoles_userRolesId",
                table: "RolesDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_ClassOfStudent_classOfStudentsId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_Subject_Faculty_facultyId",
                table: "Subject");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClass_Subject_subjectsId",
                table: "SubjectClass");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClass_Teachers_teacherId",
                table: "SubjectClass");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClassStudent_Students_studentId",
                table: "SubjectClassStudent");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClassStudent_SubjectClass_subjectClassId",
                table: "SubjectClassStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectClassStudent",
                table: "SubjectClassStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectClass",
                table: "SubjectClass");

            migrationBuilder.DropIndex(
                name: "IX_SubjectClass_subjectsId",
                table: "SubjectClass");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subject",
                table: "Subject");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RolesDetail",
                table: "RolesDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleFunction",
                table: "RoleFunction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassOfStudent",
                table: "ClassOfStudent");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassList",
                table: "ClassList");

            migrationBuilder.DropColumn(
                name: "subjectsId",
                table: "SubjectClass");

            migrationBuilder.RenameTable(
                name: "SubjectClassStudent",
                newName: "SubjectClassStudents");

            migrationBuilder.RenameTable(
                name: "SubjectClass",
                newName: "SubjectClasses");

            migrationBuilder.RenameTable(
                name: "Subject",
                newName: "Subjects");

            migrationBuilder.RenameTable(
                name: "RolesDetail",
                newName: "RolesDetails");

            migrationBuilder.RenameTable(
                name: "RoleFunction",
                newName: "RoleFunctions");

            migrationBuilder.RenameTable(
                name: "Faculty",
                newName: "Faculties");

            migrationBuilder.RenameTable(
                name: "ClassOfStudent",
                newName: "ClassOfStudents");

            migrationBuilder.RenameTable(
                name: "ClassList",
                newName: "ClassLists");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectClassStudent_subjectClassId",
                table: "SubjectClassStudents",
                newName: "IX_SubjectClassStudents_subjectClassId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectClass_teacherId",
                table: "SubjectClasses",
                newName: "IX_SubjectClasses_teacherId");

            migrationBuilder.RenameIndex(
                name: "IX_Subject_facultyId",
                table: "Subjects",
                newName: "IX_Subjects_facultyId");

            migrationBuilder.RenameIndex(
                name: "IX_RolesDetail_userRolesId",
                table: "RolesDetails",
                newName: "IX_RolesDetails_userRolesId");

            migrationBuilder.RenameIndex(
                name: "IX_RolesDetail_roleFunctionId",
                table: "RolesDetails",
                newName: "IX_RolesDetails_roleFunctionId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassList_scId",
                table: "ClassLists",
                newName: "IX_ClassLists_scId");

            migrationBuilder.AlterColumn<int>(
                name: "subjectId",
                table: "SubjectClasses",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectClassStudents",
                table: "SubjectClassStudents",
                columns: new[] { "studentId", "subjectClassId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectClasses",
                table: "SubjectClasses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RolesDetails",
                table: "RolesDetails",
                columns: new[] { "roleId", "roleFunctionId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleFunctions",
                table: "RoleFunctions",
                column: "roleFunctionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Faculties",
                table: "Faculties",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassOfStudents",
                table: "ClassOfStudents",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassLists",
                table: "ClassLists",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClasses_subjectId",
                table: "SubjectClasses",
                column: "subjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttendanceDetail_ClassLists_classListId",
                table: "AttendanceDetail",
                column: "classListId",
                principalTable: "ClassLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassLists_SubjectClasses_scId",
                table: "ClassLists",
                column: "scId",
                principalTable: "SubjectClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents",
                column: "classListId",
                principalTable: "ClassLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RolesDetails_RoleFunctions_roleFunctionId",
                table: "RolesDetails",
                column: "roleFunctionId",
                principalTable: "RoleFunctions",
                principalColumn: "roleFunctionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RolesDetails_AspNetRoles_userRolesId",
                table: "RolesDetails",
                column: "userRolesId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_ClassOfStudents_classOfStudentsId",
                table: "Students",
                column: "classOfStudentsId",
                principalTable: "ClassOfStudents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClasses_Subjects_subjectId",
                table: "SubjectClasses",
                column: "subjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClasses_Teachers_teacherId",
                table: "SubjectClasses",
                column: "teacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClassStudents_Students_studentId",
                table: "SubjectClassStudents",
                column: "studentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClassStudents_SubjectClasses_subjectClassId",
                table: "SubjectClassStudents",
                column: "subjectClassId",
                principalTable: "SubjectClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_Faculties_facultyId",
                table: "Subjects",
                column: "facultyId",
                principalTable: "Faculties",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttendanceDetail_ClassLists_classListId",
                table: "AttendanceDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassLists_SubjectClasses_scId",
                table: "ClassLists");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_RolesDetails_RoleFunctions_roleFunctionId",
                table: "RolesDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_RolesDetails_AspNetRoles_userRolesId",
                table: "RolesDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_ClassOfStudents_classOfStudentsId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClasses_Subjects_subjectId",
                table: "SubjectClasses");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClasses_Teachers_teacherId",
                table: "SubjectClasses");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClassStudents_Students_studentId",
                table: "SubjectClassStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClassStudents_SubjectClasses_subjectClassId",
                table: "SubjectClassStudents");

            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_Faculties_facultyId",
                table: "Subjects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Subjects",
                table: "Subjects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectClassStudents",
                table: "SubjectClassStudents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectClasses",
                table: "SubjectClasses");

            migrationBuilder.DropIndex(
                name: "IX_SubjectClasses_subjectId",
                table: "SubjectClasses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RolesDetails",
                table: "RolesDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoleFunctions",
                table: "RoleFunctions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Faculties",
                table: "Faculties");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassOfStudents",
                table: "ClassOfStudents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassLists",
                table: "ClassLists");

            migrationBuilder.RenameTable(
                name: "Subjects",
                newName: "Subject");

            migrationBuilder.RenameTable(
                name: "SubjectClassStudents",
                newName: "SubjectClassStudent");

            migrationBuilder.RenameTable(
                name: "SubjectClasses",
                newName: "SubjectClass");

            migrationBuilder.RenameTable(
                name: "RolesDetails",
                newName: "RolesDetail");

            migrationBuilder.RenameTable(
                name: "RoleFunctions",
                newName: "RoleFunction");

            migrationBuilder.RenameTable(
                name: "Faculties",
                newName: "Faculty");

            migrationBuilder.RenameTable(
                name: "ClassOfStudents",
                newName: "ClassOfStudent");

            migrationBuilder.RenameTable(
                name: "ClassLists",
                newName: "ClassList");

            migrationBuilder.RenameIndex(
                name: "IX_Subjects_facultyId",
                table: "Subject",
                newName: "IX_Subject_facultyId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectClassStudents_subjectClassId",
                table: "SubjectClassStudent",
                newName: "IX_SubjectClassStudent_subjectClassId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectClasses_teacherId",
                table: "SubjectClass",
                newName: "IX_SubjectClass_teacherId");

            migrationBuilder.RenameIndex(
                name: "IX_RolesDetails_userRolesId",
                table: "RolesDetail",
                newName: "IX_RolesDetail_userRolesId");

            migrationBuilder.RenameIndex(
                name: "IX_RolesDetails_roleFunctionId",
                table: "RolesDetail",
                newName: "IX_RolesDetail_roleFunctionId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassLists_scId",
                table: "ClassList",
                newName: "IX_ClassList_scId");

            migrationBuilder.AlterColumn<string>(
                name: "subjectId",
                table: "SubjectClass",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "subjectsId",
                table: "SubjectClass",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Subject",
                table: "Subject",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectClassStudent",
                table: "SubjectClassStudent",
                columns: new[] { "studentId", "subjectClassId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectClass",
                table: "SubjectClass",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RolesDetail",
                table: "RolesDetail",
                columns: new[] { "roleId", "roleFunctionId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoleFunction",
                table: "RoleFunction",
                column: "roleFunctionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Faculty",
                table: "Faculty",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassOfStudent",
                table: "ClassOfStudent",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassList",
                table: "ClassList",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClass_subjectsId",
                table: "SubjectClass",
                column: "subjectsId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttendanceDetail_ClassList_classListId",
                table: "AttendanceDetail",
                column: "classListId",
                principalTable: "ClassList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassList_SubjectClass_scId",
                table: "ClassList",
                column: "scId",
                principalTable: "SubjectClass",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassListStudents_ClassList_classListId",
                table: "ClassListStudents",
                column: "classListId",
                principalTable: "ClassList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RolesDetail_RoleFunction_roleFunctionId",
                table: "RolesDetail",
                column: "roleFunctionId",
                principalTable: "RoleFunction",
                principalColumn: "roleFunctionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RolesDetail_AspNetRoles_userRolesId",
                table: "RolesDetail",
                column: "userRolesId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_ClassOfStudent_classOfStudentsId",
                table: "Students",
                column: "classOfStudentsId",
                principalTable: "ClassOfStudent",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Faculty_facultyId",
                table: "Subject",
                column: "facultyId",
                principalTable: "Faculty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClass_Subject_subjectsId",
                table: "SubjectClass",
                column: "subjectsId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClass_Teachers_teacherId",
                table: "SubjectClass",
                column: "teacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClassStudent_Students_studentId",
                table: "SubjectClassStudent",
                column: "studentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClassStudent_SubjectClass_subjectClassId",
                table: "SubjectClassStudent",
                column: "subjectClassId",
                principalTable: "SubjectClass",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
