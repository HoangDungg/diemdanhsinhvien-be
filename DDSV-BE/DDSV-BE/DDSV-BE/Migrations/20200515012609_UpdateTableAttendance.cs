﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDSV_BE.Migrations
{
    public partial class UpdateTableAttendance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassLists_SubjectClasses_scId",
                table: "ClassLists");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents");

            migrationBuilder.DropTable(
                name: "AttendanceDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassListStudents",
                table: "ClassListStudents");

            migrationBuilder.DropColumn(
                name: "listId",
                table: "ClassListStudents");

            migrationBuilder.DropColumn(
                name: "listId",
                table: "ClassLists");

            migrationBuilder.RenameColumn(
                name: "scId",
                table: "ClassLists",
                newName: "subjectClassId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassLists_scId",
                table: "ClassLists",
                newName: "IX_ClassLists_subjectClassId");

            migrationBuilder.AddColumn<bool>(
                name: "isOpen",
                table: "SubjectClasses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "classListId",
                table: "ClassListStudents",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassListStudents",
                table: "ClassListStudents",
                columns: new[] { "studentId", "classListId" });

            migrationBuilder.CreateTable(
                name: "Attendances",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    timeSave = table.Column<string>(nullable: true),
                    classListId = table.Column<int>(nullable: false),
                    classListstudentId = table.Column<int>(nullable: true),
                    classListId1 = table.Column<int>(nullable: true),
                    isCheck = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attendances", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attendances_ClassListStudents_classListstudentId_classListId1",
                        columns: x => new { x.classListstudentId, x.classListId1 },
                        principalTable: "ClassListStudents",
                        principalColumns: new[] { "studentId", "classListId" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Faculties",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Công Nghệ Thông Tin" });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "facultyId", "subjectId", "subjectName" },
                values: new object[] { 1, 1, "CMP223", "Lập Trình Web" });

            migrationBuilder.CreateIndex(
                name: "IX_Attendances_classListstudentId_classListId1",
                table: "Attendances",
                columns: new[] { "classListstudentId", "classListId1" });

            migrationBuilder.AddForeignKey(
                name: "FK_ClassLists_SubjectClasses_subjectClassId",
                table: "ClassLists",
                column: "subjectClassId",
                principalTable: "SubjectClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents",
                column: "classListId",
                principalTable: "ClassLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClassLists_SubjectClasses_subjectClassId",
                table: "ClassLists");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents");

            migrationBuilder.DropTable(
                name: "Attendances");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClassListStudents",
                table: "ClassListStudents");

            migrationBuilder.DeleteData(
                table: "Subjects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Faculties",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "isOpen",
                table: "SubjectClasses");

            migrationBuilder.RenameColumn(
                name: "subjectClassId",
                table: "ClassLists",
                newName: "scId");

            migrationBuilder.RenameIndex(
                name: "IX_ClassLists_subjectClassId",
                table: "ClassLists",
                newName: "IX_ClassLists_scId");

            migrationBuilder.AlterColumn<int>(
                name: "classListId",
                table: "ClassListStudents",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "listId",
                table: "ClassListStudents",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "listId",
                table: "ClassLists",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClassListStudents",
                table: "ClassListStudents",
                columns: new[] { "studentId", "listId" });

            migrationBuilder.CreateTable(
                name: "AttendanceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    attendanceId = table.Column<int>(nullable: false),
                    classListId = table.Column<int>(nullable: true),
                    listId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendanceDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttendanceDetail_ClassLists_classListId",
                        column: x => x.classListId,
                        principalTable: "ClassLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttendanceDetail_classListId",
                table: "AttendanceDetail",
                column: "classListId");

            migrationBuilder.AddForeignKey(
                name: "FK_ClassLists_SubjectClasses_scId",
                table: "ClassLists",
                column: "scId",
                principalTable: "SubjectClasses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassListStudents_ClassLists_classListId",
                table: "ClassListStudents",
                column: "classListId",
                principalTable: "ClassLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
