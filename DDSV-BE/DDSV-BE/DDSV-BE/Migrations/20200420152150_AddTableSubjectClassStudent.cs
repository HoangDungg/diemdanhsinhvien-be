﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDSV_BE.Migrations
{
    public partial class AddTableSubjectClassStudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubjectClassStudent",
                columns: table => new
                {
                    studentId = table.Column<int>(nullable: false),
                    subjectClassId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectClassStudent", x => new { x.studentId, x.subjectClassId });
                    table.ForeignKey(
                        name: "FK_SubjectClassStudent_Students_studentId",
                        column: x => x.studentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectClassStudent_SubjectClass_subjectClassId",
                        column: x => x.subjectClassId,
                        principalTable: "SubjectClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClassStudent_subjectClassId",
                table: "SubjectClassStudent",
                column: "subjectClassId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubjectClassStudent");
        }
    }
}
