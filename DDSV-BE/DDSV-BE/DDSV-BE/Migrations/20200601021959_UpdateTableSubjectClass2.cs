﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDSV_BE.Migrations
{
    public partial class UpdateTableSubjectClass2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "SubjectClasses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "SubjectClasses");
        }
    }
}
