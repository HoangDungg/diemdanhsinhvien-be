﻿using DDSV_BE.DTO;
using DDSV_BE.DTO.Admin;
using DDSV_BE.Helpers;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.Binding.Student;
using DDSV_BE.Model.User;
using DDSV_BE.Response;
using DDSV_BE.Services;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DDSV_BE.Controllers
{
    [Route("api/student")]
    public class StudentController : BaseApiController
    {
        public UserManager<ApplicationUser> userManager;
        private StudentInfo studentInfo;
        protected StudentInfo TheStudentInfo
        {
            get
            {
                if (studentInfo == null)
                {
                    studentInfo = new StudentInfo(userManager);
                }
                return studentInfo;
            }
        }
        public StudentController(ddsvContext context, UserManager<ApplicationUser> userManager, IStudentService studentService) : base(context)
        {
            this.userManager = userManager;
            this.studentService = studentService;
        }


        [HttpGet]
        [Route("get-list-student")]
        public async Task<IActionResult> GetList()
        {
            var listStudent = db.Students.ToList();
            return Success(new { 
                info = listStudent
            });
        }

        [HttpGet]
        [Route("get-info-student")]
        public async Task<IActionResult> GetInfo()
        {
            var user = await this.studentService.GetCurrentStudent();
            var student = db.Students.FirstOrDefault(s => s.StudentId.ToString() == user.UserName);
            if (student == null && user == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Thông Tin Người Dùng");
            }
            return Success(new { 
                info = TheStudentInfo.Create(user,student)
            });
        }

        [HttpGet("{id}")]
        [Route("get-info-by-id")]
        public async Task<IActionResult> GetInfoById([FromQuery] int id)
        {
            var user = await this.studentService.FindByUserName(id.ToString());
            var student = db.Students.FirstOrDefault(a => a.StudentId.ToString() == user.UserName);
            if(student == null || user == null)
            {
                return Error(HttpStatusCode.NotFound, "Không tìm thấy thông tin sinh viên");
            }
            return Success(new { 
                info = TheStudentInfo.Create(user,student)
            });
        }

        [AllowAnonymous]
        //[Authorize(Roles = "Admin, Teacher")]
        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateStudentBindingModel createStudentModel)
        {
            var student = new ApplicationUser()
            {
                Email = createStudentModel.Email,
                PhoneNumber = createStudentModel.PhoneNumber,
                Student = new Student()
                {
                    StudentId = UserHelper.RandomId(),
                    FullName = createStudentModel.FullName,
                    Gender = createStudentModel.Gender,
                    DateOfBirth = createStudentModel.DateOfBirth,
                    Address = createStudentModel.Address,
                    DateOfIntoSchool = createStudentModel.DateOfIntoSchool
                },
            };
            student.UserName = student.Student.StudentId.ToString();
            createStudentModel.Password = student.Student.StudentId.ToString();

            IdentityResult addStudentResult = await this.studentService.Create(student, createStudentModel.Password);
            await db.SaveChangesAsync();
            return Success(new
            {
                info = TheStudentInfo.Create(student, student.Student)
            });
        }

        [Route("register-excel")]
        [HttpPost]
        public async Task<IActionResult> AddByExcel(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("File Not Selected");

            string fileExtension = Path.GetExtension(file.FileName);
            if (fileExtension == ".xls" || fileExtension == ".xlsx")
            {
                var fileName = file.FileName;
                var fileLocation = new FileInfo(fileName);
                //var filePath = Path.GetPathRoot(fileName);
                #region Solution 1
                //    //var rootFolder = _hostingEnvironment.WebRootPath;

                //    //var filePath = Path.Combine(rootFolder, fileName);



                //    using (var fileStream = new FileStream(filePath, FileMode.Open))
                //    {
                //        await file.CopyToAsync(fileStream);
                //    }

                //    if (file.Length <= 0)
                //        return Error(HttpStatusCode.NotFound,"Không tìm thấy file");


                //    using (ExcelPackage package = new ExcelPackage(fileLocation))
                //    {
                //        ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];
                //        //var workSheet = package.Workbook.Worksheets.First();
                //        int totalRows = workSheet.Dimension.Rows;



                //        for (int i = 3; i <= totalRows; i++)
                //        {
                //            var student = new ApplicationUser()
                //            {
                //                Email = workSheet.Cells[i, 5].Value.ToString(),
                //                Student = new Student
                //                {
                //                    FullName = workSheet.Cells[i, 1].Value.ToString(),
                //                    StudentId = UserHelper.RandomId(),
                //                    DateOfBirth = workSheet.Cells[i, 2].Value.ToString()
                //                }
                //            };
                //            student.UserName = student.Student.StudentId.ToString();
                //            IdentityResult addList = await this.studentService.Create(student, student.Student.StudentId.ToString());
                //        }

                //        await db.SaveChangesAsync();

                //    }

                #endregion

                #region Solution 2
                //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                //using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
                //{
                //    using (var reader = ExcelReaderFactory.CreateReader(stream))
                //    {
                //        while (reader.Read())
                //        {
                //            var student = new ApplicationUser()
                //            {
                //                Email = reader.GetValue(4).ToString(),
                //                Student = new Student
                //                {
                //                    FullName = reader.GetValue(0).ToString(),
                //                    StudentId = UserHelper.RandomId(),
                //                    DateOfBirth = reader.GetValue(2).ToString()
                //                }
                //            };
                //            student.UserName = student.Student.StudentId.ToString();
                //            IdentityResult addList = await this.studentService.Create(student, student.Student.StudentId.ToString());
                //        }
                //        await db.SaveChangesAsync();
                //    }
                //}
                #endregion

                #region Solution 3
                using (var stream = new MemoryStream())
                {
                    await file.CopyToAsync(stream);

                    using (var package = new ExcelPackage(stream))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];
                        var rowCount = workSheet.Dimension.Rows;

                        for(int row = 3; row <= rowCount; row++)
                        {
                            var student = new ApplicationUser()
                            {
                                Email = workSheet.Cells[row, 5].Value.ToString().Trim(),
                                Student = new Student
                                {
                                    FullName = workSheet.Cells[row, 1].Value.ToString(),
                                    StudentId = UserHelper.RandomId(),
                                    DateOfBirth = workSheet.Cells[row,2].Value.ToString()
                                }
                            };
                            student.UserName = student.Student.StudentId.ToString();
                            IdentityResult addList = await this.studentService.Create(student, student.Student.StudentId.ToString());
                        }
                        await db.SaveChangesAsync();
                    }
                }
                #endregion
            }
            return PostSuccess("Thêm Danh Sách Thành Công");
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserLogin student)
        {
            var result = await studentService.Login(student.username, student.password);
            if (result.sttCode != 0 && !string.IsNullOrEmpty(result.errMsg))
            {
                return Error(result.sttCode, result.errMsg);
            }

            return Success(new
            {
                info = TheStudentInfo.Create(result.user, result.student),
                token = result.token
            });
        }

        [Authorize(Roles = Role.student)]
        [Route("update-student")]
        [HttpPost]
        public async Task<IActionResult> Update(UpdateProfileBindingModel student)
        {
            var updateStudentResult = await this.studentService.Update(student);
            if (updateStudentResult.sttCode != 0 && !string.IsNullOrEmpty(updateStudentResult.errMsg))
            {
                return Error(updateStudentResult.sttCode, updateStudentResult.errMsg);
            }

            return PostSuccess("Cập Nhật Thông Tin Thành Công");
        }

        [Authorize(Roles = Role.student)]
        [Route("change-password-for-student")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel data)
        {
            ApplicationUser student = await this.studentService.GetCurrentStudent();

            if (student == null)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }
            IdentityResult result = await this.studentService.ChangePassword(student, data.OldPassword, data.NewPassword);

            if (!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return PostSuccess("Thay Đổi Mật Khẩu Thành Công");
        }

        [Route("student-forgot-password")]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(int id)
        {
            var result = await this.studentService.ResetPassword(id.ToString());
            if (result.result == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Tài Khoản");
            }

            if (!result.result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return Success(new
            {
                message = "Mật Khẩu Của Bạn Là:" + result.newPassword
            });
        }

        [Route("delete-student")]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]DeleteUser model)
        {
            var result = await studentService.Delete(model.Id.ToString());
            if (result == null)
            {
                return Error(HttpStatusCode.NotFound, "Tài Khoản Không Tồn Tại");
            }

            if (!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Lỗi Không Xác Định. Vui lòng thử lại sau");
            }

            return PostSuccess("Xóa thành công");
        }



        [Route("add-student-to-class-list")]
        [HttpPost]
        public async Task<IActionResult> AddStudentToClassList(ClassListStudentBindingModel model)
        {
            ClassListStudents classListStudents = new ClassListStudents
            {
                studentId = model.studentId,
                classListId = model.classListId,
            };
            if (classListStudents == null)
            {
                return Error(HttpStatusCode.BadRequest, "Có Lỗi Khi Thêm Sinh Viên Vào Danh Sách Lớp");
            }
            try
            {
                db.ClassListStudents.Add(classListStudents);
                await db.SaveChangesAsync();
                return PostSuccess("Đã Thêm Thành Công!");
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [Route("add-student-to-subject-class")]
        [HttpPost]
        public async Task<IActionResult> AddStudentToSubjectClass(SubjectClassStudentBindingModel model)
        {
            SubjectClassStudent subjectClassStudent = new SubjectClassStudent
            {
                studentId = model.studentId,
                subjectClassId = model.subjectClassId
            };

            if(subjectClassStudent == null)
            {
                return Error(HttpStatusCode.BadRequest, "Có Lỗi Khi Thêm Sinh Viên Vào Lớp Môn Học");
            }

            try
            {
                db.SubjectClassStudents.Add(subjectClassStudent);
                await db.SaveChangesAsync();
                return PostSuccess("Thêm Thành Công!");
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, "Có Lỗi Khi Thêm Sinh Viên Vào Lớp Môn Học");
            }
        }
    }
}
