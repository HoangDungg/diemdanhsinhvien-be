﻿using DDSV_BE.DTO.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Response;
using DDSV_BE.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DDSV_BE.Controllers
{
    [Route("api/users")]
    public class UserController : BaseApiController
    {
        private UserManager<ApplicationUser> userManager;
        private AdminInfo adminInfo;
        private TeacherInfo teacherInfo;
        private StudentInfo studentInfo;

        public UserController(ddsvContext context, UserManager<ApplicationUser> userManager, IAdminService adminService, ITeacherService teacherService, IStudentService studentService) : base(context)
        {
            this.userManager = userManager;
            this.studentService = studentService;
            this.adminService = adminService;
            this.teacherService = teacherService;
        }

        protected AdminInfo TheAdminInfo
        {
            get
            {
                if(adminInfo == null)
                {
                    adminInfo = new AdminInfo(userManager);
                }
                return adminInfo;
            }
        }

        protected TeacherInfo TheTeacherInfo
        {
            get
            {
                if(teacherInfo == null)
                {
                    teacherInfo = new TeacherInfo(userManager);
                }
                return teacherInfo;
            }
        }

        protected StudentInfo TheStudentInfo
        {
            get
            {
                if(studentInfo == null)
                {
                    studentInfo = new StudentInfo(userManager);
                }
                return studentInfo;
            }
        }

        [Route("get-info")]
        [HttpGet]
        public async Task<IActionResult> GetInfo()
        {
            var user = await this.adminService.GetCurrentAdmin();
            var admin = db.Admins.FirstOrDefault(a => a.adminId.ToString() == user.UserName);
            if(admin == null)
            {
                var teacher = db.Teachers.FirstOrDefault(t => t.TeacherId.ToString() == user.UserName);
                if(teacher != null)
                {
                    return Success( new { 
                        info = TheTeacherInfo.Create(user, teacher)
                    });
                }
                else
                {
                    var student = db.Students.FirstOrDefault(s => s.StudentId.ToString() == user.UserName);
                    if(student != null)
                    {
                        return Success(new { 
                            info = TheStudentInfo.Create(user,student)
                        });
                    }
                }
                return Error(HttpStatusCode.NotFound, "Không tìm thấy thông tin người dùng");
            }
            return Success(new { 
                info = TheAdminInfo.Create(user,admin)
            });
        }
        
        [Route("login")]
        [HttpPost] 
        public async Task<IActionResult> Login ([FromBody] UserLogin model)
        {
            var admin = await this.adminService.Login(model.username, model.password);
            if(admin.sttCode != 0 && !string.IsNullOrEmpty(admin.msg))
            {
                var teacher = await this.teacherService.Login(model.username, model.password);
                if(teacher.sttCode == 0 && string.IsNullOrEmpty(teacher.errMsg))
                {
                    return Success(new { 
                        info = TheTeacherInfo.Create(teacher.user, teacher.teacher),
                        token = teacher.token
                    });
                }
                else
                {
                    var student = await this.studentService.Login(model.username, model.password);
                    if(student.sttCode == 0 && string.IsNullOrEmpty(student.errMsg))
                    {
                        return Success(new
                        {
                            info = TheStudentInfo.Create(student.user, student.student),
                            token = student.token
                        });
                    }
                }
                return Error(HttpStatusCode.NotFound, "Sai tên đăng nhập hoặc mật khẩu");
            }
            return Success(new { 
                info = TheAdminInfo.Create(admin.user, admin.admin),
                token = admin.token
            });
        }
    }
}
