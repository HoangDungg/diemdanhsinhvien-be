﻿using DDSV_BE.DTO.Admin;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DDSV_BE.Model.Binding;
using DDSV_BE.Model.Binding.Teacher;
using DDSV_BE.Helpers;
using DDSV_BE.Response;
using Microsoft.EntityFrameworkCore;
using DDSV_BE.DTO;

namespace DDSV_BE.Controllers
{
    [Route("api/teachers")]
    public class TeacherController : BaseApiController
    {
        public UserManager<ApplicationUser> userManager;
        private TeacherInfo teacherInfo;

        protected TeacherInfo TheTeacherInfo
        {
            get
            {
                if(teacherInfo == null)
                {
                    teacherInfo = new TeacherInfo(userManager);
                }
                return teacherInfo;
            }
        }
        public TeacherController(ddsvContext context, UserManager<ApplicationUser> userManager, ITeacherService teacherService) : base(context)
        {
            this.userManager = userManager;
            this.teacherService = teacherService;
        }

        [HttpGet]
        [Route("get-list-teacher")]
        public async Task<IActionResult> GetList()
        {
            var listTeacher = db.Teachers.ToList();
            return Success (new {
                info = listTeacher
            });
        }

        [HttpGet]
        [Route("get-info-teacher")]
        public async Task<IActionResult> GetInfo()
        {
            var user = await this.teacherService.GetCurrentTeacher();
            var teacher = db.Teachers.FirstOrDefault(s => s.TeacherId.ToString() == user.UserName);
            if (teacher == null || user == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Thông Tin Người Dùng");
            }
            return Success(new
            {
                info = TheTeacherInfo.Create(user, teacher)
            });
        }

        [HttpGet("{id}")]
        [Route("get-info")]
        public async Task<IActionResult> GetInfo([FromQuery]string id)
        {
            var user = await this.teacherService.FindByUserName(id);
            var teacher = db.Teachers.FirstOrDefault(a => a.TeacherId.ToString() == user.UserName);
            if(user == null || teacher == null)
            {
                return Error(HttpStatusCode.NotFound, "Không tìm thấy giảng viên");
            }
            return Success(new { 
                info = TheTeacherInfo.Create(user,teacher)
            });
        }
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateTeacherBindingModel createTeacherModel)
        {
            var teacher = new ApplicationUser()
            {
                Email = createTeacherModel.Email,
                PhoneNumber = createTeacherModel.PhoneNumber,
                Teacher = new Teacher()
                {
                    TeacherId = UserHelper.RandomId(),
                    FullName = createTeacherModel.FullName,
                    Gender = createTeacherModel.Gender,
                    DateOfBirth = createTeacherModel.DateOfBirth,
                    Address = createTeacherModel.Address,
                    DateOfWork = createTeacherModel.DateOfWork,
                    RelativePhoneNumber = createTeacherModel.RelativePhone
                },
            };
            teacher.UserName = teacher.Teacher.TeacherId.ToString();
            createTeacherModel.Password = teacher.Teacher.TeacherId.ToString();

            IdentityResult addTeacherResult = await this.teacherService.Create(teacher, createTeacherModel.Password);
            await db.SaveChangesAsync();
            return Success(new
            {
                info = TheTeacherInfo.Create(teacher, teacher.Teacher)
            }) ;
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserLogin admin)
        {
            var result = await teacherService.Login(admin.username, admin.password);
            if (result.sttCode != 0 && !string.IsNullOrEmpty(result.errMsg))
            {
                return Error(result.sttCode, result.errMsg);
            }

            return Success(new
            {
                info = TheTeacherInfo.Create(result.user, result.teacher),
                token = result.token
            });
        }

        [Route("update-teacher")]
        [HttpPost]
        public async Task<IActionResult> Update(UpdateProfileBindingModel teacher)
        {
            var updateAdminResult = await this.teacherService.Update(teacher);
            if (updateAdminResult.sttCode != 0 && !string.IsNullOrEmpty(updateAdminResult.errMsg))
            {
                return Error(updateAdminResult.sttCode, updateAdminResult.errMsg);
            }

            return PostSuccess("Cập Nhật Thông Tin Thành Công");
        }
                                                
        [Route("change-password")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel data)
        {
            ApplicationUser teacher = await this.teacherService.GetCurrentTeacher();

            if (teacher == null)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }
            IdentityResult result = await this.teacherService.ChangePassword(teacher, data.OldPassword, data.NewPassword);

            if (!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return PostSuccess("Thay Đổi Mật Khẩu Thành Công");
        }

        [Route("forgot-password")]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(int id)
        {
            var result = await this.teacherService.ResetPassword(id.ToString());
            if (result.result == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Tài Khoản");
            }

            if (!result.result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return Success(new
            {
                message = "Mật Khẩu Của Bạn Là:" + result.newPassword
            });
        }

        [Route("delete-teacher")]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]DeleteUser model)
        {
            var result = await teacherService.Delete(model.Id);
            if (result == null)
            {
                return Error(HttpStatusCode.NotFound, "Tài Khoản Không Tồn Tại");
            }

            if (!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Lỗi Không Xác Định. Vui lòng thử lại sau");
            }

            return PostSuccess("Xóa thành công");
        }
    }
}
