﻿using DDSV_BE.DTO;
using DDSV_BE.Model;
using DDSV_BE.Model.Attendance;
using DDSV_BE.Model.User;
using DDSV_BE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DDSV_BE.Controllers
{
    [Route("api/schedule")]
    public class SubjectClassController : BaseApiController
    {
        public SubjectClassController(ddsvContext context, ITeacherService teacherService, IStudentService studentService): base(context) 
        {
            this.teacherService = teacherService;
            this.studentService = studentService;
        }


        [Route("teacher")]
        [HttpGet]
        public async Task<IActionResult> GetScheduleForTeacher([FromQuery]DateTime? date)
        {
            IQueryable<SubjectClass> scheduleResult = db.SubjectClasses
                .Include(s => s.classList)
                .Include(s => s.teacher)
                .Include(s => s.subjectClassStudents).AsQueryable();
            List<ScheduleDto> result = null;
            ApplicationUser user = await teacherService.GetCurrentTeacher();
            Teacher teacher = db.Teachers.FirstOrDefault(t => t.Id == user.Id);
            if(date.HasValue)
            {
                scheduleResult = scheduleResult.Where(s => s.Date == date);
            }
            try
            {
                if(teacher != null)
                {
                    scheduleResult = scheduleResult.Where(s => s.teacherId == teacher.Id);
                }

                result = scheduleResult.Select(s =>
                new ScheduleDto() 
                {
                    Id = s.Id,
                    scId = s.scId,
                    scName = s.scName,
                    roomNumber = s.roomNumber,
                    timeStart = s.timeStart,
                    numOfPerios = s.numOfPerios,
                    numOfSession = s.numOfSession,
                    belongGroup = s.belongGroup,
                    subjectId = s.subjectId,
                    subjectName =s.subject.subjectName,
                    
                    teacherId = s.teacherId,
                    teacherName =s.teacher.FullName,
                    classList = s.classList.Id
                }).ToList();
                return Success(result);
            }
            catch(Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [Route("student")]
        [HttpGet]
        public async Task<IActionResult> GetScheduleForStudent()
        {
            IQueryable<SubjectClass> scheduleResult = db.SubjectClasses
                .Include(s => s.classList)
                .Include(s => s.teacher)
                .Include(s => s.subjectClassStudents).AsQueryable();

            ApplicationUser user = await studentService.GetCurrentStudent();
            Student student = db.Students.FirstOrDefault(s => s.Id == user.Id);
            List<ScheduleDto> result = null;
            if(student != null)
            {
                scheduleResult = scheduleResult.Where(a => a.subjectClassStudents.FirstOrDefault(s => s.studentId == student.Id).studentId == student.Id);
            }
            result = scheduleResult.Select(s =>
                new ScheduleDto()
                {
                    Id = s.Id,
                    scId = s.scId,
                    scName = s.scName,
                    roomNumber = s.roomNumber,
                    timeStart = s.timeStart,
                    numOfPerios = s.numOfPerios,
                    numOfSession = s.numOfSession,
                    belongGroup = s.belongGroup,
                    subjectId = s.subjectId,
                    subjectName = s.subject.subjectName,
                    teacherId = s.teacherId,
                    teacherName = s.teacher.FullName,
                    classList = s.classList.Id,
                    subjectClassStudents = s.subjectClassStudents.Select(sc => sc.studentId)
                }).ToList();
            return Success(result);
        }

        #region FEATURE UPDATE
        public async Task<IActionResult> SwitchState(string subjectClassId)
        {
            SubjectClass result = db.SubjectClasses.Find(subjectClassId);

            if (result == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Lớp Học");
            }

            try
            {
                db.Entry(result).Property(s => s.isOpen).CurrentValue = true;
                db.Entry(result).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Ok(new
                {
                    message = "Đã mở lớp!"
                });
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        #endregion
    }
}
