﻿using DDSV_BE.DTO;
using DDSV_BE.DTO.Admin;
using DDSV_BE.Helpers;
using DDSV_BE.Model;
using DDSV_BE.Model.Binding;
using DDSV_BE.Model.Binding.Admin;
using DDSV_BE.Model.User;
using DDSV_BE.Model.UserRoles;
using DDSV_BE.Response;
using DDSV_BE.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DDSV_BE.Controllers
{
    [Route("api/admins")]
    public class AdminControllers : BaseApiController
    {
        public UserManager<ApplicationUser> userManager;
        private AdminInfo adminInfo;

        protected AdminInfo TheAdminInfo
        {
            get
            {
                if(adminInfo == null)
                {
                    adminInfo = new AdminInfo(userManager);
                }
                return adminInfo;
            }
        }
        public AdminControllers(ddsvContext context, UserManager<ApplicationUser> userManager, IAdminService adminService) : base(context)
        {
            this.userManager = userManager;
            this.adminService = adminService;
        }

        public async Task<IEnumerable<Admin>> GetList()
        {
            return db.Admins.ToList();
        }


        [HttpGet]
        [Route("get-info-admin")]
        public async Task<IActionResult> GetInfo()
        {
            var user = await this.adminService.GetCurrentAdmin();
            var admin = db.Admins.FirstOrDefault(s => s.adminId.ToString() == user.UserName);
            if (admin == null && user == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Thông Tin Người Dùng");
            }
            return Success(new
            {
                info = TheAdminInfo.Create(user, admin)
            });
        }

        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateAdminBindingModel createAdminModel)
        {
            var admin = new ApplicationUser()
            {
                Email = createAdminModel.Email,
                PhoneNumber = createAdminModel.PhoneNumber,
                Admin = new Admin()
                {
                    adminId = UserHelper.RandomId(),
                    FullName = createAdminModel.FullName,
                    Gender = createAdminModel.Gender,
                    DateOfBirth = createAdminModel.DateOfBirth,
                },
            };
            admin.UserName = admin.Admin.adminId.ToString();
            createAdminModel.Password = admin.Admin.adminId.ToString();

            IdentityResult addAdminResult = await this.adminService.Create(admin, createAdminModel.Password);
            await db.SaveChangesAsync();
                return Success(new { 
                info = TheAdminInfo.Create(admin, admin.Admin)
            });
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserLogin admin)
        {
            var result = await adminService.Login(admin.username, admin.password);
            if (result.sttCode != 0 && !string.IsNullOrEmpty(result.msg))
            {
                return Error(result.sttCode, result.msg);
            }

            return Success(
                new 
                {
                    info = TheAdminInfo.Create(result.user, result.admin),
                    token = result.token
                });
        }

        [Authorize(Roles = Role.admin)]
        [Route("update-admin")]
        [HttpPost]
        public async Task<IActionResult> Update(UpdateProfileBindingModel admin)
        {
            var updateAdminResult = await this.adminService.Update(admin);
            if (updateAdminResult.sttCode != 0 && !string.IsNullOrEmpty(updateAdminResult.errMsg))
            {
                return Error(updateAdminResult.sttCode, updateAdminResult.errMsg);
            }

            return PostSuccess("Cập Nhật Thông Tin Thành Công");
        }

        [Route("change-password")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword (ChangePasswordBindingModel data)
        {
            ApplicationUser admin = await this.adminService.GetCurrentAdmin();

            if(admin == null)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }
            IdentityResult result = await this.adminService.ChangePassword(admin, data.OldPassword, data.NewPassword);

            if(!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return PostSuccess("Thay Đổi Mật Khẩu Thành Công");
        }

        [Route("forgot-password")]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword (int id)
        {
            var result = await this.adminService.ResetPassword(id.ToString());
            if(result.result == null)
            {
                return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Tài Khoản");
            }

            if(!result.result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Đã Xảy Ra Lỗi. Vui Lòng Thử Lại Sau");
            }

            return Success(new
            {
                message = "Mật Khẩu Của Bạn Là:" + result.newPassword
            });
        }

        [Route("delete-admin")]
        [HttpPost]
        public async Task<IActionResult> Delete ([FromBody]DeleteUser model)
        {
            var result = await adminService.Delete(model.Id.ToString());
            if(result == null)
            {
                return Error(HttpStatusCode.NotFound, "Tài Khoản Không Tồn Tại");
            }

            if(!result.Succeeded)
            {
                return Error(HttpStatusCode.BadRequest, "Lỗi Không Xác Định. Vui lòng thử lại sau");
            }

            return PostSuccess("Xóa thành công");
        }
    }
}
