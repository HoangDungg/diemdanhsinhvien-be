﻿using DDSV_BE.Model;
using DDSV_BE.Model.Attendance;
using DDSV_BE.Model.Binding;
using DDSV_BE.Model.User;
using DDSV_BE.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Clauses.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace DDSV_BE.Controllers
{
    [Route("api/attendance")]
    public class AttendanceController : BaseApiController
    {
        private IHttpContextAccessor httpContextAccessor;
        public AttendanceController(ddsvContext context, IStudentService studentService, IHttpContextAccessor httpContextAccessor) : base(context)
        {
            this.studentService = studentService;
            this.httpContextAccessor = httpContextAccessor;
        }


        [Route("get-class-list")]
        [HttpGet]
        public async Task<IActionResult> GetClassList([FromQuery]int subjectClassId)
        {
            ClassList list = db.ClassLists.FirstOrDefault(c => c.subjectClassId == subjectClassId);
            if(list == null)
            {
                return Error(HttpStatusCode.NotFound, "Chưa có danh sách cho lớp học này");
            }

            return Success(list);
        }

        [Route("save")]
        [HttpPost]
        public async Task<IActionResult> Save(AttendanceBindingModel model)
        {
            List<ClassList> classList = db.ClassLists.Where(c => c.Id == model.classListId).ToList();
            var subjectClass = db.SubjectClasses.FirstOrDefault(s => s.Id == model.subjectClassId);
            if(classList == null || subjectClass == null)
            {
                return Error(HttpStatusCode.NotFound, "Không tìm thấy lớp môn học");
            }

            foreach(var item in classList)
            {
                Attendances att = new Attendances
                {
                    timeSave = DateTime.Now.ToString("hh:ss dd/MM/yyyy"),
                    StudentId = item.classListStudent.Select( a=> a.studentId).FirstOrDefault(),
                    classListId = item.classListStudent.Select(a => a.classListId).FirstOrDefault(),
                    isCheck = model.isCheck
                };
            }
            await db.SaveChangesAsync();
            return PostSuccess("Lưu danh sách thành công");
        }
        #region FEATURE
        //[Route("attendance")]
        //[HttpPost]
        //public async Task<IActionResult> StudentAttendance(int classListId, int subjectClassId)
        //{
        //    Student student = db.Students.Find(httpContextAccessor.HttpContext.User.Identity.Name);
        //    SubjectClass subjectClass = db.SubjectClasses.FirstOrDefault(a => a.Id == subjectClassId);

        //    Attendances attendance = db.Attendances
        //            .Include(a => a.classList)
        //            .Where(a => a.classList.classListId == classListId)
        //            .FirstOrDefault();

        //    if (!subjectClass.isOpen)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "Lớp học hiện chưa được mở");
        //    }


        //    if(student == null)
        //    {
        //        return Error(HttpStatusCode.NotFound, "Không Tìm Thấy Sinh Viên. Vui Lòng Thử Lại!");
        //    }

        //    if(attendance == null)
        //    {
        //        return Error(HttpStatusCode.NotFound, "Không tìm thấy danh sách lớp");
        //    }

        //    try
        //    {
        //        db.Entry(attendance).Property(a => a.isCheck).CurrentValue = true;
        //        db.Entry(attendance).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return PostSuccess("Đã Điểm Danh");

        //    }
        //    catch(Exception ex)
        //    {
        //        return Error(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}

        //[HttpPost]
        //[Route("save")]
        //public async Task<IActionResult> Save(int classListId)
        //{
        //    try
        //    {
        //        Attendances attendance = new Attendances
        //        {
        //            timeSave = DateTime.Now.ToString("dd/MM/yyyy")
        //        };

        //        await db.SaveChangesAsync();
        //        return PostSuccess("Lưu Thành Công");
        //    }
        //    catch(Exception ex)
        //    {
        //        return Error(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}
        #endregion
    }
}
